library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem"

entity subsystem_entity_ad8d961d7d is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem_entity_ad8d961d7d;

architecture structural of subsystem_entity_ad8d961d7d is
  signal ce_1_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x0: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x0 <= ce_1;
  clk_1_sg_x0 <= clk_1;
  rom_data_net_x0 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_0646f975df
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      clr => '0',
      ip => rom_data_net_x0,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem1"

entity subsystem1_entity_23cc6f88c5 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem1_entity_23cc6f88c5;

architecture structural of subsystem1_entity_23cc6f88c5 is
  signal ce_1_sg_x1: std_logic;
  signal clk_1_sg_x1: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x1: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x1 <= ce_1;
  clk_1_sg_x1 <= clk_1;
  rom_data_net_x1 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_53370f55d6
    port map (
      ce => ce_1_sg_x1,
      clk => clk_1_sg_x1,
      clr => '0',
      ip => rom_data_net_x1,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem2"

entity subsystem2_entity_8f54be1ef1 is
  port (
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic
  );
end subsystem2_entity_8f54be1ef1;

architecture structural of subsystem2_entity_8f54be1ef1 is
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic;
  signal rom_data_net_x2: std_logic_vector(7 downto 0);

begin
  rom_data_net_x2 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_1360fcde8a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => rom_data_net_x2,
      d1(0) => constant4_op_net,
      y(0) => logical_y_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem3"

entity subsystem3_entity_0cd735e7e0 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem3_entity_0cd735e7e0;

architecture structural of subsystem3_entity_0cd735e7e0 is
  signal ce_1_sg_x2: std_logic;
  signal clk_1_sg_x2: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x3: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x2 <= ce_1;
  clk_1_sg_x2 <= clk_1;
  rom_data_net_x3 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_f985bc3d72
    port map (
      ce => ce_1_sg_x2,
      clk => clk_1_sg_x2,
      clr => '0',
      ip => rom_data_net_x3,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem4"

entity subsystem4_entity_fba863f108 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem4_entity_fba863f108;

architecture structural of subsystem4_entity_fba863f108 is
  signal ce_1_sg_x3: std_logic;
  signal clk_1_sg_x3: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x4: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x3 <= ce_1;
  clk_1_sg_x3 <= clk_1;
  rom_data_net_x4 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_a848cfaa9e
    port map (
      ce => ce_1_sg_x3,
      clk => clk_1_sg_x3,
      clr => '0',
      ip => rom_data_net_x4,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem5"

entity subsystem5_entity_92451de598 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem5_entity_92451de598;

architecture structural of subsystem5_entity_92451de598 is
  signal ce_1_sg_x4: std_logic;
  signal clk_1_sg_x4: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x5: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x4 <= ce_1;
  clk_1_sg_x4 <= clk_1;
  rom_data_net_x5 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_65bd4f7f1c
    port map (
      ce => ce_1_sg_x4,
      clk => clk_1_sg_x4,
      clr => '0',
      ip => rom_data_net_x5,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem6"

entity subsystem6_entity_e74cb1baa1 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem6_entity_e74cb1baa1;

architecture structural of subsystem6_entity_e74cb1baa1 is
  signal ce_1_sg_x5: std_logic;
  signal clk_1_sg_x5: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x6: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x5 <= ce_1;
  clk_1_sg_x5 <= clk_1;
  rom_data_net_x6 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_5e26a09b2b
    port map (
      ce => ce_1_sg_x5,
      clk => clk_1_sg_x5,
      clr => '0',
      ip => rom_data_net_x6,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1/Subsystem7"

entity subsystem7_entity_f198fb2876 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in1: in std_logic_vector(7 downto 0); 
    out1: out std_logic_vector(7 downto 0)
  );
end subsystem7_entity_f198fb2876;

architecture structural of subsystem7_entity_f198fb2876 is
  signal ce_1_sg_x6: std_logic;
  signal clk_1_sg_x6: std_logic;
  signal constant4_op_net: std_logic;
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal rom_data_net_x7: std_logic_vector(7 downto 0);
  signal shift_op_net: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x6 <= ce_1;
  clk_1_sg_x6 <= clk_1;
  rom_data_net_x7 <= in1;
  out1 <= logical_y_net_x0;

  constant4: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant4_op_net
    );

  logical: entity work.logical_35d772cb43
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => shift_op_net,
      d1(0) => constant4_op_net,
      y => logical_y_net_x0
    );

  shift: entity work.shift_8848df84a2
    port map (
      ce => ce_1_sg_x6,
      clk => clk_1_sg_x6,
      clr => '0',
      ip => rom_data_net_x7,
      op => shift_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan/TextOut1"

entity textout1_entity_a34404d5d8 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    out1: out std_logic
  );
end textout1_entity_a34404d5d8;

architecture structural of textout1_entity_a34404d5d8 is
  signal ce_1_sg_x7: std_logic;
  signal clk_1_sg_x7: std_logic;
  signal constant1_op_net: std_logic_vector(2 downto 0);
  signal constant2_op_net: std_logic;
  signal constant3_op_net: std_logic;
  signal counter1_op_net: std_logic_vector(13 downto 0);
  signal counter2_op_net: std_logic_vector(2 downto 0);
  signal counter_op_net: std_logic_vector(2 downto 0);
  signal delay_q_net: std_logic_vector(2 downto 0);
  signal logical_y_net_x0: std_logic_vector(7 downto 0);
  signal logical_y_net_x1: std_logic_vector(7 downto 0);
  signal logical_y_net_x2: std_logic;
  signal logical_y_net_x3: std_logic_vector(7 downto 0);
  signal logical_y_net_x4: std_logic_vector(7 downto 0);
  signal logical_y_net_x5: std_logic_vector(7 downto 0);
  signal logical_y_net_x6: std_logic_vector(7 downto 0);
  signal logical_y_net_x7: std_logic_vector(7 downto 0);
  signal mux1_y_net_x0: std_logic;
  signal mux2_y_net: std_logic;
  signal relational_op_net: std_logic;
  signal rom_data_net_x7: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x7 <= ce_1;
  clk_1_sg_x7 <= clk_1;
  out1 <= mux1_y_net_x0;

  constant1: entity work.constant_1d6ad1c713
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant1_op_net
    );

  constant2: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant2_op_net
    );

  constant3: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant3_op_net
    );

  counter: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_786b4827bc4f1cc0",
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter_op_net
    );

  counter1: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_42641be7ce0be85d",
      op_arith => xlUnsigned,
      op_width => 14
    )
    port map (
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      clr => '0',
      en(0) => relational_op_net,
      rst => "0",
      op => counter1_op_net
    );

  counter2: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_786b4827bc4f1cc0",
      op_arith => xlUnsigned,
      op_width => 3
    )
    port map (
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter2_op_net
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      width => 3
    )
    port map (
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      d => counter2_op_net,
      en => '1',
      q => delay_q_net
    );

  mux1: entity work.mux_67acc0d350
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => constant3_op_net,
      d1(0) => mux2_y_net,
      sel(0) => constant2_op_net,
      y(0) => mux1_y_net_x0
    );

  mux2: entity work.mux_e744df0183
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => logical_y_net_x2,
      d1 => logical_y_net_x0,
      d2 => logical_y_net_x1,
      d3 => logical_y_net_x3,
      d4 => logical_y_net_x4,
      d5 => logical_y_net_x5,
      d6 => logical_y_net_x6,
      d7 => logical_y_net_x7,
      sel => delay_q_net,
      y(0) => mux2_y_net
    );

  relational: entity work.relational_8fc7f5539b
    port map (
      a => counter_op_net,
      b => constant1_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net
    );

  rom: entity work.xlsprom
    generic map (
      c_address_width => 14,
      c_width => 8,
      core_name0 => "bmg_62_d114327b534f935a",
      latency => 1
    )
    port map (
      addr => counter1_op_net,
      ce => ce_1_sg_x7,
      clk => clk_1_sg_x7,
      en => "1",
      rst => "0",
      data => rom_data_net_x7
    );

  subsystem1_23cc6f88c5: entity work.subsystem1_entity_23cc6f88c5
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x1
    );

  subsystem2_8f54be1ef1: entity work.subsystem2_entity_8f54be1ef1
    port map (
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x2
    );

  subsystem3_0cd735e7e0: entity work.subsystem3_entity_0cd735e7e0
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x3
    );

  subsystem4_fba863f108: entity work.subsystem4_entity_fba863f108
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x4
    );

  subsystem5_92451de598: entity work.subsystem5_entity_92451de598
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x5
    );

  subsystem6_e74cb1baa1: entity work.subsystem6_entity_e74cb1baa1
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x6
    );

  subsystem7_f198fb2876: entity work.subsystem7_entity_f198fb2876
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x7
    );

  subsystem_ad8d961d7d: entity work.subsystem_entity_ad8d961d7d
    port map (
      ce_1 => ce_1_sg_x7,
      clk_1 => clk_1_sg_x7,
      in1 => rom_data_net_x7,
      out1 => logical_y_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "stegan"

entity stegan is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    red1: in std_logic_vector(7 downto 0); 
    gateway_out1: out std_logic; 
    gateway_out2: out std_logic_vector(7 downto 0); 
    gateway_out3: out std_logic
  );
end stegan;

architecture structural of stegan is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "stegan,sysgen_core,{clock_period=10.00000000,clocking=Clock_Enables,compilation=ML505,sample_periods=1.00000000000,testbench=0,total_blocks=126,xilinx_adder_subtracter_block=1,xilinx_arithmetic_relational_operator_block=4,xilinx_binary_shift_operator_block=7,xilinx_bus_multiplexer_block=5,xilinx_constant_block_block=20,xilinx_counter_block=4,xilinx_delay_block=1,xilinx_gateway_in_block=1,xilinx_gateway_out_block=3,xilinx_inverter_block=2,xilinx_logical_block_block=15,xilinx_register_block=1,xilinx_single_port_read_only_memory_block=1,xilinx_system_generator_block=1,}";

  signal addsub2_s_net: std_logic_vector(1 downto 0);
  signal ce_1_sg_x8: std_logic;
  signal clk_1_sg_x8: std_logic;
  signal constant10_op_net: std_logic_vector(1 downto 0);
  signal constant11_op_net: std_logic_vector(7 downto 0);
  signal constant1_op_net: std_logic;
  signal constant3_op_net: std_logic_vector(31 downto 0);
  signal constant4_op_net: std_logic_vector(7 downto 0);
  signal constant6_op_net: std_logic_vector(1 downto 0);
  signal constant7_op_net: std_logic_vector(7 downto 0);
  signal constant8_op_net: std_logic_vector(1 downto 0);
  signal constant9_op_net: std_logic_vector(1 downto 0);
  signal counter1_op_net: std_logic_vector(31 downto 0);
  signal gateway_out1_net: std_logic;
  signal gateway_out2_net: std_logic_vector(7 downto 0);
  signal gateway_out3_net: std_logic;
  signal inverter1_op_net: std_logic;
  signal inverter_op_net: std_logic;
  signal logical1_y_net: std_logic;
  signal logical2_y_net: std_logic_vector(7 downto 0);
  signal logical3_y_net: std_logic;
  signal logical5_y_net: std_logic_vector(7 downto 0);
  signal logical6_y_net: std_logic;
  signal logical7_y_net: std_logic;
  signal logical_y_net: std_logic;
  signal mux2_y_net: std_logic_vector(1 downto 0);
  signal mux3_y_net: std_logic_vector(1 downto 0);
  signal red1_net: std_logic_vector(7 downto 0);
  signal register_q_net: std_logic_vector(7 downto 0);
  signal relational1_op_net: std_logic;
  signal relational_op_net: std_logic;

begin
  ce_1_sg_x8 <= ce_1;
  clk_1_sg_x8 <= clk_1;
  red1_net <= red1;
  gateway_out1 <= gateway_out1_net;
  gateway_out2 <= gateway_out2_net;
  gateway_out3 <= gateway_out3_net;

  addsub2: entity work.xladdsub
    generic map (
      a_arith => xlUnsigned,
      a_bin_pt => 0,
      a_width => 2,
      b_arith => xlUnsigned,
      b_bin_pt => 0,
      b_width => 2,
      c_has_c_out => 0,
      c_latency => 0,
      c_output_width => 3,
      core_name0 => "addsb_11_0_b3078c1761cd5ccf",
      extra_registers => 0,
      full_s_arith => 1,
      full_s_width => 3,
      latency => 0,
      overflow => 1,
      quantization => 1,
      s_arith => xlUnsigned,
      s_bin_pt => 0,
      s_width => 2
    )
    port map (
      a => mux2_y_net,
      b => mux3_y_net,
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      en => "1",
      s => addsub2_s_net
    );

  constant1: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net
    );

  constant10: entity work.constant_cda50df78a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant10_op_net
    );

  constant11: entity work.constant_a0a6eef0cb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant11_op_net
    );

  constant3: entity work.constant_90d08b1bd0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant3_op_net
    );

  constant4: entity work.constant_b437b02512
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant4_op_net
    );

  constant6: entity work.constant_a7e2bb9e12
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant6_op_net
    );

  constant7: entity work.constant_b437b02512
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant7_op_net
    );

  constant8: entity work.constant_cda50df78a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant8_op_net
    );

  constant9: entity work.constant_e8ddc079e9
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant9_op_net
    );

  counter1: entity work.xlcounter_free
    generic map (
      core_name0 => "cntr_11_0_6b0ce97ab3460d51",
      op_arith => xlUnsigned,
      op_width => 32
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      en => "1",
      rst => "0",
      op => counter1_op_net
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      ip(0) => relational_op_net,
      op(0) => inverter_op_net
    );

  inverter1: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      clr => '0',
      ip(0) => relational1_op_net,
      op(0) => inverter1_op_net
    );

  logical: entity work.logical_ae7040165b
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => register_q_net,
      d1 => constant4_op_net,
      y(0) => logical_y_net
    );

  logical1: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => inverter_op_net,
      d1(0) => relational1_op_net,
      y(0) => logical1_y_net
    );

  logical2: entity work.logical_a8f7ff4c23
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => register_q_net,
      d1 => constant7_op_net,
      y => logical2_y_net
    );

  logical3: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => inverter_op_net,
      d1(0) => inverter1_op_net,
      y(0) => logical3_y_net
    );

  logical5: entity work.logical_9e405cd42f
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => register_q_net,
      d1 => constant11_op_net,
      y => logical5_y_net
    );

  logical6: entity work.logical_dfe2dded7f
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational_op_net,
      d1(0) => logical3_y_net,
      y(0) => logical6_y_net
    );

  logical7: entity work.logical_dfe2dded7f
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => logical1_y_net,
      d1(0) => relational_op_net,
      y(0) => logical7_y_net
    );

  mux1: entity work.mux_f1cd62c228
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => register_q_net,
      d1 => logical2_y_net,
      d2 => logical5_y_net,
      sel => addsub2_s_net,
      y => gateway_out2_net
    );

  mux2: entity work.mux_2a63ac73aa
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => constant8_op_net,
      d1 => constant6_op_net,
      sel(0) => logical6_y_net,
      y => mux2_y_net
    );

  mux3: entity work.mux_2a63ac73aa
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => constant10_op_net,
      d1 => constant9_op_net,
      sel(0) => logical7_y_net,
      y => mux3_y_net
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 8,
      init_value => b"00000000"
    )
    port map (
      ce => ce_1_sg_x8,
      clk => clk_1_sg_x8,
      d => red1_net,
      en => "1",
      rst => "0",
      q => register_q_net
    );

  relational: entity work.relational_ee3d1b14c5
    port map (
      a(0) => logical_y_net,
      b(0) => gateway_out1_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net
    );

  relational1: entity work.relational_ee3d1b14c5
    port map (
      a(0) => constant1_op_net,
      b(0) => gateway_out1_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational1_op_net
    );

  relational3: entity work.relational_3ffd1d0a40
    port map (
      a => counter1_op_net,
      b => constant3_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => gateway_out3_net
    );

  textout1_a34404d5d8: entity work.textout1_entity_a34404d5d8
    port map (
      ce_1 => ce_1_sg_x8,
      clk_1 => clk_1_sg_x8,
      out1 => gateway_out1_net
    );

end structural;
