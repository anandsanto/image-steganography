#
# Created by System Generator     Mon Nov  9 15:12:19 2015
#
# Note: This file is produced automatically, and will be overwritten the next
# time you press "Generate" in System Generator.
#

set exename [info nameofexecutable]

set CurrentTclShell [file tail [ file rootname $exename ] ]

if { [string match "planAhead" $CurrentTclShell] } {
    source SgPaProject.tcl
} else {
    source SgIseProject.tcl
	 }

namespace eval ::xilinx::dsptool::iseproject::param {

    set Project {stegan_cw}
    set Family {Virtex5}
    set Device {xc5vlx110t}
    set Package {ff1136}
    set Speed {-1}
    set HDLLanguage {vhdl}
    set SynthesisTool {XST}
    set Simulator {Modelsim-SE}
    set ReadCores {False}
    set MapEffortLevel {High}
    set ParEffortLevel {High}
    set Frequency {100}
    set ProjectFiles {
        {{stegan_cw.vhd} -view All}
        {{stegan.vhd} -view All}
        {{stegan_cw.ucf}}
        {{bmg_62_d114327b534f935a.mif}}
        {{C:\Documents and Settings\TEMP.IIST-BC302C6023.002\My Documents\Stegan\stegan.mdl}}
    }
    set TopLevelModule {stegan_cw}
    set SynthesisConstraintsFile {stegan_cw.xcf}
    set ImplementationStopView {Structural}
    set ProjectGenerator {SysgenDSP}
}

if { [string match "planAhead" $CurrentTclShell] } {
    if { [info exists ::xilinx::dsptool::planaheadprojecttest::is_doing_planAheadGenTest] } {
        ::xilinx::dsptool::planaheadproject::compile_planahead_project
    } else {
        ::xilinx::dsptool::planaheadproject::create
    }
} else {
    ::xilinx::dsptool::iseproject::create
	 }
