
define_attribute {clk} syn_maxfan {1000000}
define_attribute {n:default_clock_driver.xlclockdriver_100000000.ce_vec*} syn_keep {true}
define_attribute {n:default_clock_driver.xlclockdriver_100000000.ce_vec*} max_fanout {"REDUCE"}

define_scope_collection ce_100000000_94a0dbaf_group \
  {find -seq * -in [ expand -hier -from {n:ce_100000000_sg_x0} ]}

define_multicycle_path -from {$ce_100000000_94a0dbaf_group} \
  -to {$ce_100000000_94a0dbaf_group} 100

# LOC constraints
define_attribute   {clk} xc_loc {Fixed}
