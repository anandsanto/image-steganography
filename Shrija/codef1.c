#include <stdio.h>
#include <math.h>
/*void firstquad(double a[40][40],int,int);
void secondquad(double a[40][40],int,int);
void thirdquad(double a[40][40],int,int);
void fourthquad(double a[40][40],int,int);*/
double b[40][40];
#define PI 3.14
void fft(int Q,double *ar,double *ai);
void ifft(int Q,double *ar,double *ai);
/*int powtwo(int in)
{
    int result = 1;
    while (result < in) result <<= 1;
    return result;
}*/
int main()
{
    #define pi 3.14159265358979323846
    double bufr[40], bufi[40];
    double fstorer[40][40],fstorei[40][40],ifstorer[40][40],ifstorei[40][40];
    double ifftr[40][40],iffti[40][40],fftr[40][40],ffti[40][40];
    double temporary[40][40];
    int func[40][40];
    int rowtemp,coltemp;
    int i,j,k1,no,row,col,grey;
    FILE *file1, *file2, *file3;
    file1=fopen("C:\Users\User\Desktop\img\store2.txt","r");
    fscanf(file1,"%d%d%d",&row,&col,&grey);
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            fscanf(file1,"%d",&no);
            func[i][j]=no;
        }
        // printf("n");
    }
    fclose(file1);
    file2=fopen("C:\Users\User\Desktop\img\out.txt","w");
    //fprintf(file2,"%d %d %d ",col,row,grey);
    file3=fopen("C:\Users\User\Desktop\img\outimag.txt","w");
    //fprintf(file3,"%d %d %d ",col,row,grey);
    for (i=0; i<row; i++)
    {
        for(j=0;j<col;j++)
        {
            bufr[j]=func[i][j];
            bufi[j]=0;
        }
        fft(col,bufr,bufi);
        for(k1=0;k1<col;k1++)
        {
            fstorer[i][k1]=bufr[k1];
            fstorei[i][k1]=bufi[k1];
        }
        // printf("n");
    }
    printf("n");
    for (i=0; i<col; i++)
    {
        for(j=0;j<row;j++)
        {
            bufr[j]=fstorer[j][i];
            bufi[j]=fstorei[j][i];
        }
        fft(row,bufr,bufi);
        for(k1=0;k1<row;k1++)
        {
            fftr[k1][i]=bufr[k1];
            ffti[k1][i]=bufi[k1];
        }
        //   printf("n");
    }
    //ifft calculation///////////////////////////////
    for (i=0; i<row; i++)
    {
        for(j=0;j<col;j++)
        {
            bufr[j]=fftr[i][j];
            bufi[j]=ffti[i][j];
        }
        ifft(col,bufr,bufi);
        for(k1=0;k1<col;k1++)
        {
            ifstorer[i][k1]=bufr[k1];
            ifstorei[i][k1]=bufi[k1];
        }
        // printf("n");
    }
    for (i=0; i<col; i++)
    {
        for(j=0;j<row;j++)
        {
            bufr[j]=ifstorer[j][i];
            bufi[j]=ifstorei[j][i];
        }
        ifft(row,bufr,bufi);
        for(k1=0;k1<row;k1++)
        {
            ifftr[k1][i]=bufr[k1];
            iffti[k1][i]=bufi[k1];
        }
        //   printf("n");
    }
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            fprintf(file3,"%d ",(sqrt(ifftr[i][j]*ifftr[i][j]+iffti[i][j]*iffti[i][j])));
        }
        fprintf(file3,"n");
    }
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            temporary[i][j]=(sqrt(fftr[i][j]*fftr[i][j]+ffti[i][j]*ffti[i][j]));
        }
        //printf("n");
    }
    printf("n");
    /*firstquad(temporary,row,col);
    secondquad(temporary,row,col);
    thirdquad(temporary,row,col);
    fourthquad(temporary,row,col);*/
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            fprintf(file2,"%dn",(long)temporary[i][j]);
        }
        printf("n");
    }
    getchar();
    return 0;
} /* main */
void fft(int N, double *ar, double *ai)
{
    #define verbose 2     /* 2 = print input and output */
    #define pi 3.14159265358979323846
    int i, j, k, L;            /* indexes */
    int M, TEMP, LE, LE1, ip;  /* M = log N */
    int NV2, NM1;
    float t;               /* temp */
    float Ur, Ui, Wr, Wi, Tr, Ti;
    float Ur_old;
    NV2 = N/2;
    NM1 = N-1;
    TEMP = N; /* get M = log N */
    M = 0;
    while(TEMP>1){
        if(TEMP%2 != 0){
            printf("Error: N not a power of 2, stoppingn");
            // exit(0);
        }
        TEMP /=2;
    M ++; }
    j = 1;
    for (i=1; i<=NM1; i++)
    { if(i<j)              /* swap a[i] and a[j] */
        { t = ar[j-1];
            ar[j-1] = ar[i-1];
            ar[i-1] = t;
            t = ai[j-1];
            ai[j-1] = ai[i-1];
        ai[i-1] = t; }
        k = NV2;             /* bit-reversed counter */
        while(k<j)
        { j -= k;
        k /= 2; }
    j += k; }
    LE = 1.;
    for (L=1; L<=M; L++)                 /* stage L */
    { LE1 = LE;                         /* LE1 = LE/2 */
        LE  *= 2;                         /* LE  = 2^L */
        Ur = 1.0;
        Ui = 0.;
        Wr = cos(PI/(float)LE1);
        Wi = -sin(PI/(float)LE1);
        for (j=1; j<=LE1; j++)
        { for (i=j; i<=N; i+=LE) /* butterfly */
            { ip = i+LE1;
                Tr = ar[ip-1]*Ur-ai[ip-1]*Ui;
                Ti = ar[ip-1]*Ui+ai[ip-1]*Ur;
                ar[ip-1] = ar[i-1] - Tr;
                ai[ip-1] = ai[i-1] - Ti;
                ar[i-1]  = ar[i-1] + Tr;
            ai[i-1]  = ai[i-1] + Ti; }  /* end of butterfly */
            Ur_old = Ur;
            Ur = Ur_old*Wr-Ui*Wi;
        Ui = Ur_old*Wi+Ui*Wr; }         /* end of j loop */
    } /* end of stage L */
} /* */
void ifft(int N, double *ar, double *ai)
{
    #define verbose 2     /* 2 = print input and output */
    int i, j, k, L;            /* indexes */
    int M, TEMP, LE, LE1, ip;  /* M = log N */
    int NV2, NM1;
    double t;               /* temp */
    double Ur, Ui, Wr, Wi, Tr, Ti;
    double Ur_old;
    NV2 = N/2;
    NM1 = N-1;
    TEMP = N; /* get M = log N */
    M = 0;
    while(TEMP>1){
        if(TEMP%2 != 0){
            printf("Error: N not a power of 2, stoppingn");
            // exit(0);
        }
        TEMP /=2;
    M ++; }
    /* shuffle */
    j = 1;
    for (i=1; i<=NM1; i++)
    { if(i<j)              /* swap a[i] and a[j] */
        { t = ar[j-1];
            ar[j-1] = ar[i-1];
            ar[i-1] = t;
            t = ai[j-1];
            ai[j-1] = ai[i-1];
        ai[i-1] = t; }
        k = NV2;             /* bit-reversed counter */
        while(k<j)
        { j -= k;
        k /= 2; }
    j += k; }
    #define PI 3.14
    LE = 1.;
    for (L=1; L<=M; L++)                 /* stage L */
    { LE1 = LE;                         /* LE1 = LE/2 */
        LE  *= 2;                         /* LE  = 2^L */
        Ur = 1.0;
        Ui = 0.;
        Wr = cos(PI/(double)LE1);
        Wi = sin(PI/(double)LE1);
        for (j=1; j<=LE1; j++)
        { for (i=j; i<=N; i+=LE) /* butterfly */
            { ip = i+LE1;
                Tr = ar[ip-1]*Ur-ai[ip-1]*Ui;
                Ti = ar[ip-1]*Ui+ai[ip-1]*Ur;
                ar[ip-1] = ar[i-1] - Tr;
                ai[ip-1] = ai[i-1] - Ti;
                ar[i-1]  = ar[i-1] + Tr;
            ai[i-1]  = ai[i-1] + Ti; }  /* end of butterfly */
            Ur_old = Ur;
            Ur = Ur_old*Wr-Ui*Wi;
        Ui = Ur_old*Wi+Ui*Wr; }         /* end of j loop */
    } /* end of stage L */
}
/*void fourthquad(double a[40][40],int row,int col)
{
    int i,j;
    int rowt=row;
    int colt=col-1;
    int temp;
    for(i=((row)/2);i<row;i++)
    {
        for(j=((col)/2);j<col;j++)
        {
            b[i][j]=a[(rowt-1)][(colt)];
            colt--;
        }
        // printf("n");
        rowt--;
        colt=col-1;
    }
    rowt=row;
    colt=col-1;
    for(i=((row)/2);i<row;i++)
    {
        for(j=((col)/2);j<col;j++)
        {
            a[i][j]=b[i][j];
        }
        //  printf("n");
    }
}
void secondquad(double a[40][40],int row,int col)
{
    int i,j;
    int colt,rowt;
    colt=col-1;
    rowt=(row/2)-1;
    for(i=0;i<(row/2);i++)
    {
        for(j=(col/2);j<col;j++)
        {                     // printf(" %d%d *",i,j);
            //printf(" %d%d ",j,i);
            b[rowt][colt]=a[i][j];
            colt--;
            //  printf(" %d ",b[i][j]);
        }
        colt=col-1;
        rowt--;
        // printf("n");
    }
    for(i=0;i<(row/2);i++)
    {
        for(j=(col/2);j<col;j++)
        {
            a[i][j]=b[i][j];
        }
        //printf("n");
    }
}
void thirdquad(double a[40][40],int row,int col)
{
    int i,j;
    int rowt,colt;
    rowt=row/2;
    colt=0;
    for(i=(row/2);i<row;i++)
    {
        for(j=0;j<(col/2);j++)
        {
            // printf(" %d%d ",rowt,colt);
            b[rowt][colt]=a[i][j];
            rowt++;
        }
        // printf("n");
        rowt=row/2;
        colt++;
    }
    for(i=(row/2);i<row;i++)
    {
        for(j=0;j<(col/2);j++)
        {
            a[i][j]=b[i][j];
        }
        //printf("n");
    }
}
void firstquad(double a[40][40],int row,int col)
{
    int i,j;
    int rowt,colt;
    rowt=(row/2)-1;
    colt=(col/2)-1;
    for(i=0;i<(row/2);i++)
    {
        for(j=0;j<(col/2);j++)
        {
            //  printf(" %d%d ",i,j);
            b[rowt][colt]=a[i][j];
            colt--;
        }
        //  printf("n");
        rowt--;
        colt=(col/2)-1;
    }
    for(i=0;i<(row/2);i++)
    {
        for(j=0;j<(col/2);j++)
        {
            a[i][j]=b[i][j];
        }
        //printf("n");
    }
}   */
